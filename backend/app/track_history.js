const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const Track = require('../models/Track');
const auth = require("../middleware/auth");
const permit = require('../middleware/permit');
const User = require("../models/User");
const router = express.Router();

router.get('/', auth, async (req, res) => {
    try {
        const token = req.get('Authorization');
        const userTracksHistory = await TrackHistory.find({userId: req.user._id})
            .populate({path: 'trackId', select: 'name', populate: {path: 'album', select: 'artist', populate: {path: 'artist', select: 'name'}}})
            .sort({datetime: -1});

       const publishedUserTrackHistory = userTracksHistory.filter(trackHistory => trackHistory.published === true);

       if (!token) {
           return res.send(publishedUserTrackHistory);
       }

        const user = await User.findOne({token});

        if (user.role === 'user') {
            return res.send(publishedUserTrackHistory);
        }

        return res.send(userTracksHistory);
    } catch (error) {
        return res.status(404).send('Not Found');
    }
})

router.post('/', auth, async (req, res) => {
    const track = await Track.findById(req.body.track);

    if (!track) {
        return res.status(404).send({error: 'Track is not found'});
    }

    const trackHistoryData = {
        userId: req.user._id,
        trackId: req.body.track,
    }

    const trackHistory = new TrackHistory(trackHistoryData);

    try {
        await trackHistory.save();
        res.send(trackHistory);
    } catch {
        res.status(400).send({error: 'Data not valid'});
    }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
   try {
        const track = await TrackHistory.findById(req.params.id);

        if (!track) {
            return res.status(404).send({error: 'Not Found'});
        }

        track.published = !track.published;

       await track.save();
       res.send(track);
   } catch  {
       return res.sendStatus(500);
   }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const track = await TrackHistory.findByIdAndDelete(req.params.id);

        if (!track) {
            return res.status(404).send({error: 'Not Found'});
        }

        res.send(track);
    } catch (error) {
        return res.sendStatus(500);
    }
});

module.exports = router;