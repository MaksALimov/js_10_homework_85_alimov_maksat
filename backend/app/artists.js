const express = require('express');
const Artist = require('../models/Artist');
const User = require('../models/User');
const config = require('../config');
const {nanoid} = require('nanoid');
const multer = require('multer');
const path = require('path');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },

    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const router = express.Router();

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const artists = await Artist.find();

        const token = req.get('Authorization');
        const publishedArtists = artists.filter(artist => artist.published === true);

        if (!token) {
            return res.send(publishedArtists);
        }

        const user = await User.findOne({token});

        if (user.role === 'user') {
            return res.send(publishedArtists);
        }

        res.send(artists);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        const artistData = {
            name: req.body.name,
            information: req.body.information || null,
        };

        if (req.file) {
            artistData.image = 'uploads/' + req.file.filename;
        }

        const artist = new Artist(artistData);
        await artist.save();
        res.send(artist);
    } catch (e) {
        res.status(400).send({error: e.message});
    }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const artist = await Artist.findById(req.params.id);

        if (!artist) {
            return res.status(404).send({error: 'Not Found'});
        }

        artist.published = !artist.published;

        await artist.save();
        res.send(artist);
    } catch (error) {
        return res.sendStatus(500);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const artist = await Artist.findByIdAndDelete(req.params.id);

        if (!artist) {
            return res.status(404).send({error: 'Not Found'});
        }

        res.send(artist);
    } catch (error) {
        res.sendStatus(500);
    }
});

module.exports = router;