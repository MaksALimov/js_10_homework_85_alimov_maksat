const express = require('express');
const User = require('../models/User');
const router = express.Router();
const config = require('../config');
const axios = require("axios");
const {nanoid} = require('nanoid');

router.post('/', async (req, res) => {
    try {
        const userData = {
            email: req.body.email,
            password: req.body.password,
            displayName: req.body.displayName,
            avatarImage: req.body.avatarImage || null,
        };

        const user = new User(userData);

        user.generateToken();
        await user.save();
        res.send(user);
    } catch (error) {
        res.status(400).send(error);
    }
});

router.post('/sessions', async (req, res) => {
    try {
        const user = await User.findOne({email: req.body.email});

        if (!user) {
            return res.status(401).send({message: 'Data not valid'});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(401).send({message: 'Data not valid'});
        }

        user.generateToken();
        await user.save({validateBeforeSave: false});
        res.send(user);
    } catch (error) {
        return res.sendStatus(500);
    }
});

router.post('/facebookLogin', async (req, res) => {
    const inputToken = req.body.accessToken;
    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);

        if (response.data.data.error) {
            return res.status(401).send({global: 'Facebook token incorrect'});
        }

        if (req.body.id !== response.data.data.user_id) {
            return res.status(401).send({global: 'Wrong user id'});
        }

        let user = await User.findOne({email: req.body.email});

        if (!user) {
            user = await User.findOne({facebookId: req.body.id});
        }

        if (!user) {
            user = new User({
                email: req.body.email,
                password: nanoid(),
                facebookId: req.body.id,
                displayName: req.body.name,
                avatarImage: req.body.picture.data.url,
            });
        }

        user.generateToken();
        await user.save({validateBeforeSave: false});
        res.send(user);

    } catch (error) {
        res.status(401).send({global: 'Facebook token incorrect'});
    }
});


router.delete('/sessions', async (req, res) => {
    try {
        const token = req.get('Authorization');
        const success = {message: 'Success'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.generateToken();

        await user.save({validateBeforeSave: false});

        return res.send(success);
    } catch (error) {
        return res.sendStatus(500)
    }
});

module.exports = router;