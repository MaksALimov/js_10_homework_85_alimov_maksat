const express = require('express');
const Track = require('../models/Track');
const auth = require("../middleware/auth");
const permit = require('../middleware/permit');
const User = require("../models/User");
const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const token = req.get('Authorization');
        const albumId = {};

        if (req.query.album) {
            albumId.album = req.query.album;
            const tracksAlbum = await Track.find(albumId).populate('album', 'name artist').sort({trackNumber: 1});

            const publishedTracksAlbum = tracksAlbum.filter(trackAlbum => trackAlbum.published === true);
            if (token === 'null') {
                return res.send(publishedTracksAlbum);
            }

            const user = await User.findOne({token});

            if (user.role === 'user') {
                return res.send(publishedTracksAlbum);
            }

            return res.send(tracksAlbum);
        }

        const tracks = await Track.find().sort({trackNumber: 1});
        res.send(tracks);
    } catch {
        res.sendStatus(500);
    }
});

router.post('/', auth, permit('admin'), async (req, res) => {
    try {
        const trackData = {
            name: req.body.name,
            album: req.body.album || null,
            duration: req.body.duration || null,
            trackNumber: req.body.trackNumber,
        };

        const track = new Track(trackData);

        await track.save();
        res.send(track);
    } catch (e) {
        res.status(400).send({error: e});
    }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
   try {
       const track = await Track.findById(req.params.id);

       if (!track) {
           return res.status(404).send({error: 'Not Found'});
       }

       track.published = !track.published;

       await track.save();
       res.send(track);
   } catch (error) {
       return res.sendStatus(500);
   }

});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const track = await Track.findByIdAndDelete(req.params.id);

        if (!track) {
            return res.status(404).send({error: 'Not Found'});
        }

        res.send(track);
    } catch (error) {
        res.sendStatus(500);
    }
});

module.exports = router;