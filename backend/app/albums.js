const express = require('express');
const Album = require('../models/Album');
const {nanoid} = require('nanoid');
const multer = require('multer');
const path = require('path');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const config = require("../config");
const User = require("../models/User");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },

    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const router = express.Router();

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const token = req.get('Authorization');
        const artistAlbums = {};

        if (req.query.artist) {
            artistAlbums.artist = req.query.artist;
            const albums = await Album.find(artistAlbums).populate('artist', 'name').sort('year 1');
            const publishedAlbums = albums.filter(album => album.published === true);

            if (!token) {
                return res.send(publishedAlbums);
            }

            const user = await User.findOne({token});

            if (user.role === 'user') {
                return res.send(publishedAlbums);
            }
        }

        const albums = await Album.find();
        res.send(albums);

    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const token = req.get('Authorization');
        const album = await Album.findById(req.params.id).populate('artist', 'name information image');

        if (!token) {
            if (String(album._id) === req.params.id) {
                return album.published ? res.send(album) : res.send({message: 'This album do not published'});
            }
        }

        const user = await User.findOne({token});

        if (user.role === 'user') {
            if (album.published === true) {
                return res.send(album);
            }
        }

        if (album) {
            res.send(album);
        } else {
            res.status(404).send({error: 'Product not found'});
        }

    } catch {
        res.status(500);
    }
});

router.post('/', auth, permit('admin'), upload.single('image'), async (req, res) => {
    try {
        const albumData = {
            name: req.body.name,
            year: req.body.year || null,
            artist: req.body.artist || null,
        };

        if (req.file) {
            albumData.image = 'uploads/' + req.file.filename;
        }

        const album = new Album(albumData);

        await album.save();
        res.send(album);
    } catch (e) {
        res.status(400).send({error: e});
    }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const album = await Album.findById(req.params.id);

        if (!album) {
            return res.status(404).send({error: 'Not Found'});
        }
        album.published = !album.published;

        await album.save();
        res.send(album);

    } catch (error) {
        return res.sendStatus(500);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const album = await Album.findByIdAndDelete(req.params.id);

        if (!album) {
            return res.status(404).send({error: 'Not Found'});
        }

        res.send(album);

    } catch {
        res.sendStatus(500);
    }
})

module.exports = router;