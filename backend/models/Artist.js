const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },

    published: {
        type: Boolean,
        required: true,
        default: false
    },

    image: String,
    information: String,
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;