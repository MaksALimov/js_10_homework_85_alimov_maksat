const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require('./models/User');
const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const TrackHistory = require('./models/TrackHistory');

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const collection of collections) {
        await mongoose.connection.db.dropCollection(collection.name);
    }

    const [firstArtist, secondArtist] = await Artist.create({
        name: 'Avicii',
        image: 'fixtures/avicii.jpg',
        information: 'Some information',
        published: true,
    }, {
        name: 'Freddie Mercury',
        image: 'fixtures/freddie.jpg',
        information: 'A very good singer',
        published: true,
    });

    const [firstAlbum, secondAlbum] = await Album.create({
        name: 'True',
        artist: firstArtist,
        year: 2013,
        image: 'fixtures/true.jpg',
        published: true,
    }, {
        name: 'Queen_II',
        artist: secondArtist,
        year: 1973,
        image: 'fixtures/Queen_ii.png',
        published: true,
    });

    const [firstTrack, secondTrack, thirdTrack, fourthTrack, fifthTrack, sixthTrack, seventhTrack, eightTrack, ninthTrack, tenthTrack] = await Track.create({
        name: 'Levels',
        album: firstAlbum,
        duration: '3:18',
        trackNumber: 1,
        published: true,
    }, {
        name: 'Wake me up',
        album: firstAlbum,
        duration: '4:32',
        trackNumber: 2,
        published: false,
    }, {
        name: 'Waiting for love',
        album: firstAlbum,
        duration: '3:50',
        trackNumber: 3,
        published: false,
    }, {
        name: 'The Nights',
        album: firstAlbum,
        duration: '3:11',
        trackNumber: 4,
        published: false,
    }, {
        name: 'Hey Brother',
        album: firstAlbum,
        duration: '4:18',
        trackNumber: 5,
        published: false,
    }, {
        name: 'Radio Ga Ga',
        album: secondAlbum,
        duration: '5:53',
        trackNumber: 1,
        published: false,
    }, {
        name: 'Don\'t stop me now',
        album: secondAlbum,
        duration: '3:32',
        trackNumber: 2,
        published: false,
    }, {
        name: 'I want to break free',
        album: secondAlbum,
        duration: '3:45',
        trackNumber: 3,
        published: false,
    }, {
        name: 'We will rock you',
        album: secondAlbum,
        duration: '2:14',
        trackNumber: 4,
        published: true,
    }, {
        name: 'Bohemian Rhapsody',
        album: secondAlbum,
        duration: '5:59',
        trackNumber: 5,
        published: true,
    });

    const [firstUser, secondUser] = await User.create({
        email: 'admin@gmail.com',
        password: '123',
        token: nanoid(),
        role: 'admin',
        displayName: 'Admin',
        avatarImage: 'fixtures/noimage.jpg',
    }, {
        email: 'user@gmail.com',
        password: '123',
        token: nanoid(),
        role: 'user',
        displayName: 'User',
        avatarImage: 'fixtures/noimage.jpg',
    });

    await TrackHistory.create({
        userId: firstUser,
        trackId: firstTrack,
        datetime: new Date("11/24/2020 11:25"),
        published: false,
    }, {
        userId: secondUser,
        trackId: secondTrack,
        datetime: new Date("11/24/2020 11:28"),
        published: false
    }, {
        userId: firstUser,
        trackId: thirdTrack,
        datetime: new Date("11/24/2020 12:25"),
        published: true,
    }, {
        userId: secondUser,
        trackId: fourthTrack,
        datetime: new Date("11/24/2020 13:25"),
        published: false,
    }, {
        userId: firstUser,
        trackId: fifthTrack,
        datetime: new Date("11/24/2020 14:25"),
        published: true,
    }, {
        userId: firstUser,
        trackId: seventhTrack,
        datetime: new Date("11/24/2020 15:25"),
        published: false,
    }, {
        userId: secondUser,
        trackId: eightTrack,
        datetime: new Date("11/24/2020 16:25"),
        published: true,
    }, {
        userId: firstUser,
        trackId: ninthTrack,
        datetime: new Date("11/24/2020 11:25"),
        published: false,
    }, {
        userId: secondUser,
        trackId: tenthTrack,
        datetime: new Date("11/24/2020 12:25"),
        published: true,
    });

    await mongoose.connection.close();
};

run().catch(e => console.log(e));