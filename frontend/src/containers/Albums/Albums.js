import React, {Fragment, useEffect} from 'react';
import {useLocation} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {Button, CircularProgress, Grid, makeStyles, Typography} from "@material-ui/core";
import ArtistAlbums from "../../components/ArtistAlbums/ArtistAlbums";
import {getArtistAlbums} from "../../store/actions/albumsActions";

const useStyles = makeStyles(() => ({
    artist: {
        fontSize: '40px',
        textTransform: 'uppercase',
        margin: '40px 0',
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center'
    },

    noAlbum: {
        fontSize: '40px',
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: '30px',
    },

    getBackButton: {
        marginBottom: '40px',
    },

    progressContainer: {
        textAlign: 'center',
    },

    notFound: {
        fontSize: '35px',
        fontWeight: 'bold',
        textAlign: 'center',
    },
}));

const Albums = ({history}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const location = useLocation();
    const artistAlbums = useSelector(state => state.albums.artistAlbums);
    const loading = useSelector(state => state.albums.artistAlbumsLoading);
    const error = useSelector(state => state.albums.error);

    useEffect(() => {
        dispatch(getArtistAlbums(location.search));
    }, [dispatch, location.search]);

    const getBack = () => {
        history.goBack();
    };


    return artistAlbums.length > 0 ? (
        <Grid item>
            <Grid item>
                <Typography variant="h5" className={classes.artist}>{artistAlbums[0].artist.name}</Typography>
            </Grid>
            <Grid item container>
                <Button variant="contained" className={classes.getBackButton} onClick={getBack}>
                    Back
                </Button>
            </Grid>
            <Grid item className={classes.progressContainer}>
                {loading ? <CircularProgress/> : null}
            </Grid>
            {artistAlbums.map(artistAlbum => (
                <Fragment key={artistAlbum._id}>
                    <ArtistAlbums
                        name={artistAlbum.name}
                        image={artistAlbum.image}
                        year={artistAlbum.year}
                        id={artistAlbum._id}
                        published={artistAlbum.published}
                    />
                </Fragment>
            ))}
        </Grid>
    ) : error ? <Typography className={classes.notFound}>{error}</Typography> :
        <Grid container direction="column" alignItems="center">
            <Typography className={classes.noAlbum}>У этого исполнителя нет альбомов</Typography>
            <Button variant="contained" onClick={getBack}>
                Back
            </Button>
        </Grid>;
};

export default Albums;