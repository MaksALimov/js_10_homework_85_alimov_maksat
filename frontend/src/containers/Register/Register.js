import React, {useEffect, useState} from 'react';
import {Button, Container, Grid, makeStyles, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {clearTextFieldsError, registerUser} from "../../store/actions/usersActions";
import FormElement from "../../components/UI/Form/FormElement";

const useStyles = makeStyles(() => ({
    signUp: {
        textAlign: 'center',
        fontSize: '30px',
        color: '#fff',
        fontWeight: 'bold',
        marginBottom: '20px',
    },

    signIn: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: '20px',
        margin: '20px 0',
        textDecoration: 'none',
    },
}));

const Register = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.users.registerError);

    useEffect(() => {
        return () => {
            dispatch(clearTextFieldsError());
        };
    }, [dispatch]);

    const [user, setUser] = useState({
        email: '',
        password: '',
        displayName: '',
        avatarImage: '',
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setUser(prevState => ({...prevState, [name]: value}));
    };

    const onSubmitForm = e => {
        e.preventDefault();
        dispatch(registerUser({...user}));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (error) {
            return undefined;
        }
    };

    return (
        <Container maxWidth="xs">
            <Grid container direction="column" component="form" onSubmit={onSubmitForm} noValidate>
                <Grid item>
                    <Typography
                        component="h2"
                        variant="h6"
                        className={classes.signUp}
                    >
                        Sign up
                    </Typography>
                </Grid>
                <FormElement
                    type="text"
                    required
                    autoComplete="new-email"
                    label="Email"
                    name="email"
                    value={user.email}
                    onChange={onInputChange}
                    error={getFieldError('email')}
                >
                </FormElement>
                <FormElement
                    type="password"
                    required
                    autoComplete="new-password"
                    label="Password"
                    name="password"
                    value={user.password}
                    onChange={onInputChange}
                    error={getFieldError('password')}
                >
                </FormElement>

                <FormElement
                    type="text"
                    required
                    autoComplete="new-displayName"
                    label="DisplayName"
                    name="displayName"
                    value={user.displayName}
                    onChange={onInputChange}
                    error={getFieldError('displayName')}
                />

                <FormElement
                    type="text"
                    autoComplete="new-avatarImage"
                    label="AvatarImage"
                    name="avatarImage"
                    value={user.avatarImage}
                    onChange={onInputChange}
                    error={getFieldError('avatarImage')}
                />

                <Grid item>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                    >
                        Sign up
                    </Button>
                </Grid>
                <Grid item container justifyContent="flex-end">
                    <Link variant="body2" to="/login" className={classes.signIn}>
                        Already have an account ? Sign in
                    </Link>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Register;