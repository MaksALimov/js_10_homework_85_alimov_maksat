import React, {useEffect, useState} from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {getAlbums} from "../../store/actions/albumsActions";
import {addTrack} from "../../store/actions/tracksActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {clearTextFieldsError} from "../../store/actions/usersActions";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },

    submitButton: {
        textAlign: 'center',
    },

    errors: {
        fontSize: '40px',
        textAlign: 'center',
    }
}));


const AddTrack = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const albums = useSelector(state => state.albums.albums);
    const addTrackError = useSelector(state => state.tracks.addTrackError);
    const addTrackLoading = useSelector(state => state.tracks.addTrackLoading);

    useEffect(() => {
        dispatch(getAlbums());
        return () => {
            dispatch(clearTextFieldsError());
        };
    }, [dispatch]);


    const getFieldError = field => {
        try {
            return addTrackError.error.errors[field].message;
        } catch (e) {
            return undefined;
        }
    };

    const [track, setTrack] = useState({
        name: '',
        album: '',
        duration: '',
        trackNumber: '',
    });

    const submitFormHandler = e => {
        e.preventDefault();

        dispatch(addTrack({...track}));
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setTrack(prevState => {
            return {...prevState, [name]: value};
        });
    };

    return (
        typeof addTrackError === 'string' ? <Typography className={classes.errors}>{addTrackError}</Typography> : (
            <Grid container
                  direction="column"
                  spacing={2}
                  component="form"
                  className={classes.root}
                  onSubmit={submitFormHandler}
            >
                <FormElement
                    select
                    options={albums}
                    label="Albums"
                    onChange={inputChangeHandler}
                    name="album"
                    value={track.album}
                />

                <FormElement
                    label="Name"
                    onChange={inputChangeHandler}
                    name="name"
                    value={track.name}
                    error={getFieldError('name')}
                />

                <FormElement
                    label="Duration"
                    onChange={inputChangeHandler}
                    name="duration"
                    value={track.duration}
                />

                <FormElement
                    type="number"
                    label="Track Number"
                    onChange={inputChangeHandler}
                    name="trackNumber"
                    value={track.trackNumber}
                    error={getFieldError('trackNumber')}
                />

                <Grid item xs className={classes.submitButton}>
                    <ButtonWithProgress
                        type="submit"
                        color="primary"
                        variant="contained"
                        loading={addTrackLoading}
                        disabled={addTrackLoading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>)
    );
};

export default AddTrack;