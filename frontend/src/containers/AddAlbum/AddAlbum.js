import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getArtists} from "../../store/actions/artistsActions";
import FormElement from "../../components/UI/Form/FormElement";
import {Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import {addAlbum} from "../../store/actions/albumsActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {clearTextFieldsError} from "../../store/actions/usersActions";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },

    submitButton: {
        textAlign: 'center',
    },

    errors: {
        fontSize: '40px',
        textAlign: 'center',
    }
}));

const AddAlbum = () => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const artists = useSelector(state => state.artists.artists);
    const addAlbumError = useSelector(state => state.albums.addAlbumError);
    const addAlbumLoading = useSelector(state => state.albums.addAlbumLoading);

    useEffect(() => {
        dispatch(getArtists());
        return () => {
            dispatch(clearTextFieldsError());
        };
    }, [dispatch]);


    const getFieldError = field => {
        try {
            return addAlbumError.error.errors[field].message;
        } catch (e) {
            return undefined;
        }
    };

    const [album, setAlbum] = useState({
        name: '',
        artist: '',
        year: '',
        image: null,
    });

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(album).forEach(key => {
            formData.append(key, album[key]);
        });

        dispatch(addAlbum(formData));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setAlbum(prevState => {
            return {...prevState, [name]: file}
        });
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setAlbum(prevState => {
            return {...prevState, [name]: value};
        });
    };


    return (
        typeof addAlbumError === 'string' ? <Typography className={classes.errors}>{addAlbumError}</Typography> : (
            <Grid container
                  direction="column"
                  spacing={2}
                  component="form"
                  className={classes.root}
                  onSubmit={submitFormHandler}
            >
                <FormElement
                    select
                    options={artists}
                    label="Artists"
                    onChange={inputChangeHandler}
                    name="artist"
                    value={album.artist}
                    error={getFieldError('artist')}
                />

                <FormElement
                    label="Name"
                    onChange={inputChangeHandler}
                    name="name"
                    value={album.name}
                    error={getFieldError('name')}
                />

                <FormElement
                    label="Year"
                    type="number"
                    onChange={inputChangeHandler}
                    name="year"
                    value={album.year}
                />

                <Grid item xs>
                    <TextField
                        type="file"
                        name="image"
                        variant="outlined"
                        fullWidth
                        onChange={fileChangeHandler}
                    />
                </Grid>

                <Grid item xs className={classes.submitButton}>
                    <ButtonWithProgress
                        type="submit"
                        color="primary"
                        variant="contained"
                        loading={addAlbumLoading}
                        disabled={addAlbumLoading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>)
    );
};

export default AddAlbum;