import React, {useEffect, useState} from 'react';
import {Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {addArtist} from "../../store/actions/artistsActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {clearTextFieldsError} from "../../store/actions/usersActions";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },

    submitButton: {
        textAlign: 'center',
    },

    errors: {
        fontSize: '40px',
        textAlign: 'center',
    }
}));

const AddArtist = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const addArtistError = useSelector(state => state.artists.addArtistError);
    const addArtistLoading = useSelector(state => state.artists.addArtistLoading);

    useEffect(() => {
        dispatch(clearTextFieldsError());
    }, [dispatch]);

    const getFieldError = () => {
        try {
            return addArtistError.error;
        } catch (e) {
            return undefined;
        }
    };

    const [artist, setArtist] = useState({
        name: '',
        image: null,
        information: '',
    });

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(artist).forEach(key => {
            formData.append(key, artist[key]);
        });

        dispatch(addArtist(formData));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setArtist(prevState => {
            return {...prevState, [name]: file}
        });
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setArtist(prevState => {
            return {...prevState, [name]: value};
        });
    };

    return (
        typeof addArtistError === 'string' ? <Typography className={classes.errors}>{addArtistError}</Typography> : (
            <Grid container
                  direction="column"
                  spacing={2}
                  component="form"
                  className={classes.root}
                  onSubmit={submitFormHandler}
            >
                <FormElement
                    label="Name"
                    onChange={inputChangeHandler}
                    name="name"
                    value={artist.name}
                    error={getFieldError('name')}
                />

                <FormElement
                    label="Information"
                    onChange={inputChangeHandler}
                    name="information"
                    value={artist.information}
                />

                <Grid item xs>
                    <TextField
                        type="file"
                        name="image"
                        variant="outlined"
                        fullWidth
                        onChange={fileChangeHandler}
                    />
                </Grid>

                <Grid item xs className={classes.submitButton}>
                    <ButtonWithProgress
                        type="submit"
                        color="primary"
                        variant="contained"
                        loading={addArtistLoading}
                        disabled={addArtistLoading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>)
    );
};

export default AddArtist;