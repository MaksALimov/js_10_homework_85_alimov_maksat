import React, {Fragment, useEffect} from 'react';
import {useLocation} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getAlbumTracks} from "../../store/actions/tracksActions";
import {getArtists} from "../../store/actions/artistsActions";
import {Button, CircularProgress, Grid, makeStyles, Typography} from "@material-ui/core";
import Track from "../../components/Track/Track";

const useStyles = makeStyles(() => ({
    artistName: {
        textAlign: 'center',
        marginBottom: '50px',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        color: '#fff',
    },

    albumName: {
        textAlign: 'center',
        marginBottom: '50px',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        fontSize: '25px',
        color: '#fff',
    },

    trackHeaders: {
        fontSize: '30px',
        color: '#0d1137',
        fontWeight: 'bold',
        marginBottom: '50px',
        marginRight: '40px',
    },

    trackHeaderName: {
        marginRight: '280px',
    },

    durationContainer: {
        marginRight: '85px',
    },

    noTrack: {
        fontSize: '35px',
        fontWeight: 'bold',
    },

    progressContainer: {
        textAlign: 'center',
        margin: '40px 0',
    },

    notFound: {
        fontSize: '35px',
        fontWeight: 'bold',
        textAlign: 'center',
    },
}));

const Tracks = ({history}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const location = useLocation();
    const albumTracks = useSelector(state => state.tracks.albumTracks);
    const artists = useSelector(state => state.artists.artists);
    const loading = useSelector(state => state.tracks.loading);
    const error = useSelector(state => state.tracks.error);
    const user = useSelector(state => state.users.user);

    let artistName = null;

    if (albumTracks.length > 0) {
        artistName = artists.find(artist => artist._id === albumTracks[0].album.artist);
    }

    useEffect(() => {
        dispatch(getArtists());
        dispatch(getAlbumTracks(location.search));
    }, [dispatch, location.search]);

    const getBack = () => {
        history.goBack();
    };

    return (
        <>
            <Grid item>
                <Grid item className={classes.notFound}>{error ? error : albumTracks.length > 0 ?
                    <Grid container direction="column">
                        <Grid item>
                            <Button variant="contained" onClick={getBack}>Back</Button>
                            <Typography
                                variant="h4"
                                className={classes.artistName}>
                                {artistName && artistName.name}
                            </Typography>
                            <Typography
                                variant="h4"
                                className={classes.albumName}
                            >
                                {albumTracks[0].album.name}
                            </Typography>
                            <Grid item className={classes.progressContainer}>
                                {loading ? <CircularProgress/> : null}
                            </Grid>
                        </Grid>
                        <Grid container item>
                            <Grid container justifyContent="space-between">
                                <Grid item>
                                    <Typography
                                        variant="h5"
                                        className={classes.trackHeaders}>
                                        Track Number
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography
                                        variant="h5"
                                        className={`${classes.trackHeaders} 
                                        ${classes.durationContainer}`}>
                                        Duration
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography
                                        variant="h5"
                                        className={`${user && user.role === 'user' ?
                                            classes.trackHeaders :
                                            `${classes.trackHeaders} ${classes.trackHeaderName}`}`}
                                    >Name</Typography>
                                </Grid>
                                {user && user.role === 'admin' ? <Grid item>
                                    <Typography variant="h5" className={classes.trackHeaders}>Add</Typography>
                                </Grid> : null}
                            </Grid>
                        </Grid>
                        <Grid item>
                            {albumTracks.map(track => (
                                <Fragment key={track._id}>
                                    <Track
                                        trackNumber={track.trackNumber}
                                        name={track.name}
                                        duration={track.duration}
                                        trackId={track._id}
                                        published={track.published}
                                        albumId={track.album._id}
                                    />
                                </Fragment>
                            ))}
                        </Grid>
                    </Grid> : <Grid container direction="column" alignItems="center">
                        <Typography
                            className={classes.noTrack}>
                            У этого альбома нет треков
                        </Typography>
                        <Button variant="contained" onClick={getBack}>Back</Button>
                    </Grid>}</Grid>
            </Grid>
        </>
    );
};

export default Tracks;