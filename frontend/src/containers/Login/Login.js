import React, {useEffect, useState} from 'react';
import {Container, Grid, makeStyles, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {clearTextFieldsError, loginUser} from "../../store/actions/usersActions";
import {Alert} from "@material-ui/lab";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import FacebookLogin from "../../components/UI/FacebookLogin/FacebookLogin";

const useStyles = makeStyles(() => ({
    signUp: {
        textAlign: 'center',
        fontSize: '30px',
        color: '#fff',
        fontWeight: 'bold',
        marginBottom: '20px',
    },

    signIn: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: '20px',
        margin: '20px 0',
        textDecoration: 'none',
    },

    signInBtn: {
      marginTop: '20px',
    },

    alert: {
        marginTop: '24px',
        width: '100%',
    },
}));

const Login = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.users.loginError);
    const loading = useSelector(state => state.users.loginLoading);

    useEffect(() => {
        return () => {
            dispatch(clearTextFieldsError());
        };
    }, [dispatch]);

    const [user, setUser] = useState({
        email: '',
        password: '',
    });

    const onInputChange = e => {
        const {name, value} = e.target;
        setUser(prevState => ({...prevState, [name]: value}));
    };

    const onSubmitForm = async e => {
        e.preventDefault();
        await dispatch(loginUser({...user}));
    };

    return (
        <Container maxWidth="xs">
            <Grid container direction="column" component="form" onSubmit={onSubmitForm}>
                <Grid item>
                    <Typography
                        component="h2"
                        variant="h6"
                        className={classes.signUp}
                    >
                        Sign in
                    </Typography>
                    {error && <Alert severity="error" className={classes.alert}>
                        {error.message || error.global}
                    </Alert>}
                </Grid>
                <FormElement
                    type="text"
                    autoComplete="new-email"
                    label="Email"
                    name="email"
                    value={user.email}
                    onChange={onInputChange}
                >
                </FormElement>
                <FormElement
                    type="password"
                    autoComplete="new-password"
                    label="Password"
                    name="password"
                    value={user.password}
                    onChange={onInputChange}
                >
                </FormElement>

                <Grid item>
                    <FacebookLogin/>
                </Grid>

                <Grid item>
                    <ButtonWithProgress
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        loading={loading}
                        disabled={loading}
                        className={classes.signInBtn}
                    >
                        Sign in
                    </ButtonWithProgress>
                </Grid>
                <Grid item container justifyContent="flex-end">
                    <Link variant="body2" to="/register" className={classes.signIn}>
                        Or sign up
                    </Link>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Login;