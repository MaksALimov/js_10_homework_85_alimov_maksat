import React, {Fragment, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import UserTrackHistory from "../../components/UserTrackHistory/userTrackHistory";
import {Button, makeStyles, Typography} from "@material-ui/core";
import {Redirect} from "react-router-dom";
import {getUserTrackHistory} from "../../store/actions/trackHistoryActions";

const useStyles = makeStyles(() => ({
    notFound: {
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: '40px',
    },
}));

const TrackHistory = ({history}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const userTracksHistory = useSelector(state => state.trackHistory.userTrackHistory);
    const user = useSelector(state => state.users.user);
    const error = useSelector(state => state.trackHistory.userTrackHistoryError);

    console.log(error);
    useEffect(() => {
        dispatch(getUserTrackHistory());
    }, [dispatch]);

    if (!user) {
        return <Redirect to={'/'}/>
    }

    return (
        <>
            <Button variant="contained" onClick={() => history.goBack()}>
                Back
            </Button>
            {error ? <Typography className={classes.notFound}>{error}</Typography> : (
                userTracksHistory.map(userTrack => (
                    <Fragment key={userTrack._id}>
                        <UserTrackHistory
                            artistName = {userTrack.trackId.album.artist ? userTrack.trackId.album.artist.name : null}
                            trackName={userTrack.trackId.name}
                            datetime={userTrack.datetime}
                            trackId={userTrack._id}
                            published={userTrack.published}
                            id={userTrack._id}
                        />
                    </Fragment>
                ))

            )}
        </>
    );
};

export default TrackHistory;