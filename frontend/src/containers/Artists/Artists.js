import React, {Fragment, useEffect} from 'react';
import {CircularProgress, Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getArtists} from "../../store/actions/artistsActions";
import Artist from "../../components/Artist/Artist";

const useStyles = makeStyles(() => ({
    container: {
        minHeight: '80%',
        boxShadow: 'rgba(0, 0, 0, 0.2) 0px 12px 28px 0px, rgba(0, 0, 0, 0.1) 0px 2px 4px 0px, rgba(255, 255, 255, 0.05) 0px 0px 0px 1px inset;',
        borderRadius: '10px',
        backgroundColor: '#62639B'
    },

    progressContainer: {
        margin: '40px 0'
    },

    noArtists: {
      fontSize: '40px',
      textAlign: 'center',
    },
}));

const Artists = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);
    const loading = useSelector(state => state.artists.artistsLoading);
    const error = useSelector(state => state.artists.error);

    useEffect(() => {
        dispatch(getArtists());
    }, [dispatch]);

    return (
        artists.length > 0 ? <Grid container className={classes.container}>
            <Grid item container direction="column" alignItems="center">
                <Typography variant="h3">{error}</Typography>
            </Grid>
            <Grid item container direction="column" alignItems="center" className={classes.progressContainer}>
                {loading ? <CircularProgress/> : null}
            </Grid>
            {artists.map(artist => (
                <Fragment key={artist._id}>
                    <Artist
                        name={artist.name}
                        image={artist.image}
                        id={artist._id}
                        published={artist.published}
                    />
                </Fragment>
            ))}
        </Grid> : <Typography className={classes.noArtists}>No Artists</Typography>
    );
};

export default Artists;