import React from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {makeTrackHistory} from "../../store/actions/trackHistoryActions";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {deleteTrack, publishTrack} from "../../store/actions/tracksActions";
import {useLocation} from "react-router-dom";

const useStyles = makeStyles(() => ({
    tracksInfoContainer: {
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
        width: '100px',
        height: 'auto',
        textAlign: 'center',
        margin: '15px 0'
    },

    tracksInfo: {
        fontSize: '24px',
    },

    tracksName: {
        fontWeight: 'bold',
        fontSize: '16px',
        textTransform: 'capitalize',
        marginTop: '35px',
    },

    noUserTracksName: {
        marginRight: '140px',
        fontSize: '20px',
    },

    deleteButton: {
        margin: '0 20px',
    },

    error: {
        fontSize: '35px',
        textAlign: 'center',
    }

}));

const Track = ({trackNumber, duration, name, trackId, published, albumId}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const location = useLocation().search;
    const user = useSelector(state => state.users.user);
    const trackHistoryLoading = useSelector(state => state.trackHistory.trackHistoryLoading);
    const publishTrackLoading = useSelector(state => state.tracks.publishTrackLoading);
    const publishTrackError = useSelector(state => state.tracks.publishTrackError);
    const deleteTrackLoading = useSelector(state => state.tracks.deleteTrackLoading);
    const deleteTrackError = useSelector(state => state.tracks.deleteTrackError);

    const addTrack = (trackId, name) => {
        dispatch(makeTrackHistory(trackId, name));
    }

    return (
        <>
            {deleteTrackError || publishTrackError ? (
                <Typography className={classes.error}>{deleteTrackError || publishTrackError}</Typography>
            ) : user && user.role === 'admin' ? (
                    <Grid container justifyContent="space-between" alignItems="center">
                        <Grid item className={classes.tracksInfoContainer}>
                            <Typography variant="body1" className={classes.tracksInfo}>{trackNumber}</Typography>
                        </Grid>
                        <Grid item className={classes.tracksInfoContainer}>
                            <Typography variant="body1" className={classes.tracksInfo}>{duration}</Typography>
                        </Grid>
                        {user ?
                            <>
                                <Grid container item className={classes.tracksInfoContainer}>
                                    <ButtonWithProgress
                                        variant="contained"
                                        className={classes.tracksName}
                                        onClick={() => addTrack(trackId, name)}
                                        loading={trackHistoryLoading}
                                        disabled={trackHistoryLoading}>
                                        {name}
                                    </ButtonWithProgress>
                                    <Grid item className={classes.tracksInfoContainer}>
                                        {published ? null : <Typography variant="body1">Unpublished</Typography>}
                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <ButtonWithProgress
                                        variant="contained"
                                        className={classes.deleteButton}
                                        onClick={() => dispatch(deleteTrack(trackId, name))}
                                        loading={deleteTrackLoading}
                                        disabled={deleteTrackLoading}
                                    >
                                        Delete
                                    </ButtonWithProgress>
                                    <ButtonWithProgress
                                        variant="contained"
                                        onClick={() => dispatch(publishTrack(trackId, location))}
                                        loading={publishTrackLoading}
                                        disabled={publishTrackLoading}
                                    >
                                        {published ? <Typography>Unpublished</Typography> :
                                            <Typography>Publish</Typography>}
                                    </ButtonWithProgress>
                                </Grid>
                            </> :
                            <Typography className={classes.tracksName}>{name}</Typography>}
                    </Grid>
                ) :
                published ? (
                    <Grid container justifyContent="space-between" alignItems="center">
                        <Grid item className={classes.tracksInfoContainer}>
                            <Typography variant="body1" className={classes.tracksInfo}>{trackNumber}</Typography>
                        </Grid>
                        <Grid item className={classes.tracksInfoContainer}>
                            <Typography variant="body1" className={classes.tracksInfo}>{duration}</Typography>
                        </Grid>
                        {user ? <Grid item className={classes.tracksInfoContainer}>
                            <ButtonWithProgress
                                variant="contained"
                                className={classes.tracksName}
                                onClick={() => addTrack(trackId, name)}
                                loading={trackHistoryLoading}
                                disabled={trackHistoryLoading}
                            >
                                {name}
                            </ButtonWithProgress>
                        </Grid> : <Typography
                            className={user ? classes.tracksName : classes.noUserTracksName}>{name}</Typography>}
                    </Grid>
                ) : null}
        </>
    );
};

export default Track;