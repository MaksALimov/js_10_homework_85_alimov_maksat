import React from 'react';
import dayjs from "dayjs";
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {deleteUserTrackHistory, publishTrackHistory} from "../../store/actions/trackHistoryActions";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(() => ({
    userTrackHistory: {
        fontSize: '24px',
        width: '300px',
        textAlign: 'center',
    },

    artistName: {
        fontSize: '26px',
        fontWeight: 'bold',
        color: '#fff',
    },

    published: {
        marginLeft: '75px',
    },

    errors: {
        fontSize: '25px',
        textAlign: 'center',
    },
}));

const UserTrackHistory = ({artistName, trackName, datetime, trackId, published, id}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    const deleteUserTrackHistoryLoading = useSelector(state => state.trackHistory.deleteUserTrackHistoryLoading);
    const deleteUserTrackHistoryError = useSelector(state => state.trackHistory.deleteUserTrackHistoryError);
    const publishUserTrackHistoryLoading = useSelector(state => state.trackHistory.publishUserTrackHistoryLoading);
    const publishUserTrackHistoryError = useSelector(state => state.trackHistory.publishUserTrackHistoryError);

    return (
        <>
            {deleteUserTrackHistoryError || publishUserTrackHistoryError ? (
                    <Typography className={classes.errors}>{deleteUserTrackHistoryError || publishUserTrackHistoryError}</Typography>
                ) :
                user && user.role === 'admin' ?
                    <Grid container justifyContent="space-around" alignItems="center" spacing={2}>
                        <Grid item className={classes.userTrackHistory}>
                            <Typography className={classes.artistName}>{artistName}</Typography>
                        </Grid>
                        <Grid item className={classes.userTrackHistory}>
                            <Typography>
                                {trackName}
                            </Typography>
                        </Grid>
                        <Grid item className={classes.userTrackHistory}>
                            <Typography>
                                {dayjs(datetime).format('YYYY-MM-DD HH:mm:ss')}
                            </Typography>
                        </Grid>
                        <Grid item>
                            {published ? <Typography className={classes.published}/> :
                                <Typography>Unpublished</Typography>}
                        </Grid>
                        <ButtonWithProgress
                            variant="contained"
                            onClick={() => dispatch(publishTrackHistory(id))}
                            loading={publishUserTrackHistoryLoading}
                            disabled={publishUserTrackHistoryLoading}
                        >
                            {published ? <Typography>Unpublished</Typography> : <Typography>Publish</Typography>}
                        </ButtonWithProgress>
                        <Grid item>
                            <ButtonWithProgress
                                variant="contained"
                                onClick={() => dispatch(deleteUserTrackHistory(trackId, trackName))}
                                loading={deleteUserTrackHistoryLoading}
                                disabled={deleteUserTrackHistoryLoading}
                            >
                                Delete
                            </ButtonWithProgress>
                        </Grid>
                    </Grid> : published ?
                        <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
                            <Grid item className={classes.userTrackHistory}>
                                <Typography className={classes.artistName}>{artistName}</Typography>
                            </Grid>
                            <Grid item className={classes.userTrackHistory}>
                                <Typography>
                                    {trackName}
                                </Typography>
                            </Grid>
                            <Grid item className={classes.userTrackHistory}>
                                <Typography>
                                    {dayjs(datetime).format('YYYY-MM-DD HH:mm:ss')}
                                </Typography>
                            </Grid>
                        </Grid> : null
            }
        </>
    );
};

export default UserTrackHistory;