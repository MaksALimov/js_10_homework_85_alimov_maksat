import React from 'react';
import {Card, CardMedia, Grid, makeStyles, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import imageNotAvailable from '../../assets/images/noImage.jpeg';
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deleteArtist, publishArtist} from "../../store/actions/artistsActions";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(() => ({
    container: {
        padding: '20px 70px',
    },

    image: {
        width: '100%',
        minHeight: '100%',
    },

    imageContainer: {
        width: '300px',
        height: '300px',
        padding: '20px',
    },

    artistName: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: '30px',
        padding: '15px',
        textDecoration: 'none',
        '&:hover': {
            cursor: 'pointer'
        },
    },

    deleteButton: {
        margin: '10px 0',
    },

    unpublished: {
        marginBottom: '20px',
        fontSize: '25px',
        textAlign: 'center',
    },

    errors: {
        fontSize: '30px',
        textAlign: 'center',
        margin: '0 10px',
    }
}));

const Artist = ({image, name, id, published}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const deleteArtistLoading = useSelector(state => state.artists.deleteArtistLoading);
    const deleteArtistError = useSelector(state => state.artists.deleteArtistError);
    const publishArtistLoading = useSelector(state => state.artists.publishArtistLoading);
    const publishArtistError = useSelector(state => state.artists.publishArtistError);

    let artistImage = imageNotAvailable;

    if (image) {
        artistImage = apiURL + '/' + image;
    }

    return (
        <>
            {deleteArtistError || publishArtistError ? (
                <Grid container justifyContent="center">
                    <Typography
                        className={classes.errors}>{deleteArtistError || publishArtistError}
                    </Typography></Grid>
            ) : user && user.role === 'admin' ?
                <Grid container justifyContent="space-between" alignItems="center" className={classes.container}>
                    <Grid item>
                        <Typography variant="h6">
                            <Link to={`/albums?artist=${id}`} className={classes.artistName}>
                                {name}
                            </Link>
                        </Typography>
                    </Grid>
                    <Grid item>
                        {published ? null : <Typography className={classes.unpublished}>Unpublished</Typography>}
                        <Card className={classes.imageContainer}>
                            <CardMedia
                                className={classes.image}
                                image={artistImage}
                            />
                        </Card>
                        <Grid item container alignItems="center" justifyContent="space-evenly">
                            <ButtonWithProgress
                                variant="contained"
                                className={classes.deleteButton}
                                onClick={() => dispatch(deleteArtist(id, name))}
                                loading={deleteArtistLoading}
                                disabled={deleteArtistLoading}
                            >
                                Delete
                            </ButtonWithProgress>
                            <ButtonWithProgress
                                variant="contained"
                                onClick={() => dispatch(publishArtist(id))}
                                loading={publishArtistLoading}
                                disabled={publishArtistLoading}
                            >
                                {published ? <Typography>Unpublished</Typography> : <Typography>Publish</Typography>}
                            </ButtonWithProgress>
                        </Grid>
                    </Grid>
                </Grid> : published ?
                    <Grid container justifyContent="space-between" alignItems="center" className={classes.container}>
                        <Grid item>
                            <Typography variant="h6">
                                <Link to={`/albums?artist=${id}`} className={classes.artistName}>
                                    {name}
                                </Link>
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Card className={classes.imageContainer}>
                                <CardMedia
                                    className={classes.image}
                                    image={artistImage}
                                />
                            </Card>
                        </Grid>
                    </Grid> : null}
        </>
    );
};

export default Artist;