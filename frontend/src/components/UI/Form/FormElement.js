import React from 'react';
import PropTypes from 'prop-types';
import {Grid, makeStyles, MenuItem, TextField} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    textField: {
        margin: '10px 0',
        color: '#fff',
        fontWeight: 'bold',
        fontSize: '20px',
    },
}));

const FormElement = ({label, name, value, onChange, required, error, autoComplete, type, select, options, multiline, helperText}) => {
    const classes = useStyles();

    let inputChildren = null;

    if (select) {
        inputChildren = options.map(option => (
            <MenuItem key={option._id} value={option._id}>
                {option.name}
            </MenuItem>
        ));
    }

    return (
        <Grid item>
            <TextField
                select={select}
                type={type}
                required={required}
                multiline={multiline}
                autoComplete={autoComplete}
                label={label}
                variant="outlined"
                fullWidth
                name={name}
                className={classes.textField}
                inputProps={{className: classes.textField}}
                value={value}
                onChange={onChange}
                error={Boolean(error)}
                helperText={error}
            >
                {inputChildren}
            </TextField>
        </Grid>
    );
};

FormElement.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    required: PropTypes.bool,
    error: PropTypes.string,
    autoComplete: PropTypes.string,
    type: PropTypes.string,
    select: PropTypes.bool,
    options: PropTypes.arrayOf(PropTypes.object),
    multiline: PropTypes.bool,
    helperText: PropTypes.string,
};

export default FormElement;