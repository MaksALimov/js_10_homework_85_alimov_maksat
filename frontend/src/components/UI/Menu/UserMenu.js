import React, {useState} from 'react';
import {Button, makeStyles, Menu, MenuItem} from "@material-ui/core";
import {Link} from "react-router-dom";

const useStyles = makeStyles(() => ({
    addMenu: {
        fontSize: '25px',
        color: '#fff',
        fontWeight: 'bold',
        margin: '0 15px',
    },
}));

const UserMenu = () => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div>
            <Button className={classes.addMenu} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                Add
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem>
                    <Link to="/add-artist">
                        Artist
                    </Link>
                </MenuItem>
                <MenuItem>
                    <Link to="/add-album">
                        Album
                    </Link>
                </MenuItem>
                <MenuItem>
                    <Link to="/add-track">
                        Track
                    </Link>
                </MenuItem>
            </Menu>
        </div>
    );
};

export default UserMenu;