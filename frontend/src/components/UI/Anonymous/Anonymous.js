import React from 'react';
import {Button, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";

const useStyles = makeStyles(() => ({
    register: {
        color: '#fff',
        fontSize: '20px',
        textDecoration: 'none',
    },

    login: {
        color: '#fff',
        fontSize: '20px',
        textDecoration: 'none',
    }
}));

const Anonymous = () => {
    const classes = useStyles();
    return (
        <>
            <Button component={Link} to="/register" className={classes.register}>Sign up</Button>
            <Button component={Link} to="/login" className={classes.register}>Sign in</Button>
        </>
    );
};

export default Anonymous;