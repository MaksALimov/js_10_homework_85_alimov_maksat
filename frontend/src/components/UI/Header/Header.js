import React from 'react';
import {AppBar, Button, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import Anonymous from "../Anonymous/Anonymous";
import {logoutUser} from "../../../store/actions/usersActions";
import UserMenu from "../Menu/UserMenu";
import Grid from "@material-ui/core/Grid";
import {apiURL} from "../../../config";
import imageNotAvailable from '../../../assets/images/noImage.jpeg';

const useStyles = makeStyles(() => ({
    mainLink: {
        color: 'inherit',
        textDecoration: 'none',
        '&:hover': {
            color: 'inherit'
        }
    },

    register: {
        color: '#fff',
        fontSize: '20px',
        textDecoration: 'none',
    },

    login: {
        color: '#fff',
        fontSize: '20px',
        textDecoration: 'none',
    },

    trackHistoryButton: {
        color: '#fff',
        boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
        padding: '10px',
        fontWeight: 'bold',
        cursor: 'pointer'
    },

    logout: {
        fontSize: '25px',
        color: '#fff',
        fontWeight: 'bold',
    },

    displayName: {
        fontSize: '35px',
        fontWeight: 'bold',
        margin: '0 30px',
    },

    imageContainer: {
        width: '110px',
        height: '110px',
    },

    img: {
        width: '100%',
        height: '100%',
    },
}))

const AppToolbar = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    let avatarImage = user?.avatarImage;

    if (!user?.avatarImage) {
        avatarImage = imageNotAvailable;
    }

    if (/fixtures/g.test(user?.avatarImage)) {
        avatarImage = apiURL + '/' + user?.avatarImage;
    }

    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Grid container justifyContent="space-between" alignItems="center">
                        <Grid item>
                            <Typography variant="h6">
                                <Link to="/" className={classes.mainLink}>Computer parts shop</Link>
                            </Typography>
                        </Grid>
                        <Grid item>
                            {user ? (
                                <Grid container justifyContent="space-between" alignItems="center">
                                    <Grid item className={classes.imageContainer}>
                                        <img src={avatarImage} alt="userAvatar" className={classes.img}/>
                                    </Grid>
                                    <Typography className={classes.displayName}>
                                        Hello {user.displayName} !
                                    </Typography>
                                    <Button
                                        className={classes.trackHistoryButton}
                                        component={Link} to="/track-history">
                                        Track History
                                    </Button>
                                    <UserMenu/>
                                    <Button
                                        className={classes.logout}
                                        onClick={() => dispatch(logoutUser())}>
                                        Logout
                                    </Button>
                                </Grid>
                            ) : (
                                <Anonymous/>
                            )}
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar/>
        </>
    );
};

export default AppToolbar;