import React from 'react';
import {apiURL} from "../../config";
import {Grid, makeStyles, Typography} from "@material-ui/core";
import noAlbumImage from '../../assets/images/noImage.jpeg';
import {Link, useLocation} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deleteAlbum, publishAlbum} from "../../store/actions/albumsActions";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(() => ({
    spacing: {
        margin: '30px 0',
    },

    albumName: {
        fontSize: '30px',
        fontWeight: 'bold',
        color: '#fff',
        textDecoration: 'none',
        '&:hover': {
            cursor: 'pointer',
        }
    },

    albumNameContainer: {
        display: "flex",
        alignItems: 'center',
        textAlign: 'center',
        width: '200px',
        height: '200px',
    },

    imageContainer: {
        width: '200px',
        height: '200px',
    },

    image: {
        width: '100%',
        height: '100%',
    },

    year: {
        fontSize: '30px',
        fontWeight: 'bold',
    },

    unpublished: {
        fontSize: '24px',
        marginTop: '15px',
    },

    errors: {
        fontSize: '40px',
        textAlign: 'center',
    }
}));

const ArtistAlbums = ({name, image, year, id, published}) => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);
    const dispatch = useDispatch();
    const location = useLocation().search;
    const loading = useSelector(state => state.albums.publishAlbumLoading);
    const publishAlbumError = useSelector(state => state.albums.publishAlbumError);
    const deleteAlbumLoading = useSelector(state => state.albums.deleteAlbumLoading);
    const deleteAlbumError = useSelector(state => state.albums.deleteAlbumError);

    let albumImage = noAlbumImage;

    if (image) {
        albumImage = apiURL + '/' + image;
    }

    return (
        <>
            {publishAlbumError ? <Typography className={classes.errors}>{publishAlbumError}</Typography> : deleteAlbumError ?
                <Typography className={classes.errors}>{deleteAlbumError}</Typography> : user && user.role === 'admin' ?
                    <Grid container justifyContent="center">
                        <Grid container justifyContent="space-between" alignItems="center">
                            <Grid item className={classes.spacing}>
                                <div className={classes.albumNameContainer}>
                                    <Typography variant="h5">
                                        <Link to={`/tracks?album=${id}`} className={classes.albumName}>
                                            {name}
                                        </Link>
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item className={classes.imageContainer}>
                                <img src={albumImage} className={classes.image} alt="albumImg"/>
                                <Grid container item direction="column" alignItems="center">
                                    {published ? null :
                                        <Typography className={classes.unpublished}>Unpublished</Typography>}
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Typography variant="h5" className={classes.year}>{year}</Typography>
                            </Grid>
                            <Grid item>
                                <ButtonWithProgress
                                    variant="contained"
                                    onClick={() => dispatch(publishAlbum(id, location))}
                                    loading={loading}
                                    disabled={loading}
                                >
                                    {published ? <Typography>Unpublished</Typography> :
                                        <Typography>Publish</Typography>}
                                </ButtonWithProgress>
                            </Grid>
                            <Grid item>
                                <ButtonWithProgress
                                    variant="contained"
                                    onClick={() => dispatch(deleteAlbum(id))}
                                    loading={deleteAlbumLoading}
                                    disabled={deleteAlbumLoading}
                                >
                                    Delete
                                </ButtonWithProgress>
                            </Grid>
                        </Grid>
                    </Grid> : published ? <Grid container justifyContent="center">
                        <Grid container justifyContent="space-between" alignItems="center">
                            <Grid item className={classes.spacing}>
                                <div className={classes.albumNameContainer}>
                                    <Typography variant="h5">
                                        <Link to={`/tracks?album=${id}`} className={classes.albumName}>
                                            {name}
                                        </Link>
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item className={classes.imageContainer}>
                                <img src={albumImage} className={classes.image} alt="albumImg"/>
                            </Grid>
                            <Grid item>
                                <Typography variant="h5" className={classes.year}>{year}</Typography>
                            </Grid>
                        </Grid>
                    </Grid> : null}
        </>
    );
};

export default ArtistAlbums;