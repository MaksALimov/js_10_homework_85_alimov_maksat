import {Redirect, Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import AddArtist from "./containers/AddArtist/AddArtist";
import AddAlbum from "./containers/AddAlbum/AddAlbum";
import AddTrack from "./containers/AddTrack/AddTrack";
import {useSelector} from "react-redux";

const App = () => {
    const user = useSelector(state => state.users.user);

    const AddForm = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Artists}/>
                <Route path="/albums" component={Albums}/>
                <Route path="/tracks" component={Tracks}/>
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <Route path="/track-history" component={TrackHistory}/>
                <AddForm
                    isAllowed={user?.role === 'admin' || user?.role === 'user'}
                    path="/add-artist"
                    component={AddArtist}
                    redirectTo="/"
                />
                <AddForm
                    isAllowed={user?.role === 'admin' || user?.role === 'user'}
                    path="/add-album"
                    component={AddAlbum}
                    redirectTo="/"
                />
                <AddForm
                    isAllowed={user?.role === 'admin' || user?.role === 'user'}
                    path="/add-track"
                    component={AddTrack}
                    redirectTo="/"
                />
            </Switch>
        </Layout>
    )
};

export default App;
