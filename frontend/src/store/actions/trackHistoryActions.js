import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";

export const TRACK_HISTORY_REQUEST = 'TRACK_HISTORY_REQUEST';
export const TRACK_HISTORY_SUCCESS = 'TRACK_HISTORY_SUCCESS';
export const TRACK_HISTORY_FAILURE = 'TRACK_HISTORY_FAILURE';

export const USER_TRACK_HISTORY_REQUEST = 'USER_TRACK_HISTORY_REQUEST';
export const USER_TRACK_HISTORY_SUCCESS = 'USER_TRACK_HISTORY_SUCCESS';
export const USER_TRACK_HISTORY_FAILURE = 'USER_TRACK_HISTORY_FAILURE';

export const DELETE_USER_TRACK_HISTORY_REQUEST = 'DELETE_USER_TRACK_HISTORY_REQUEST';
export const DELETE_USER_TRACK_HISTORY_SUCCESS = 'DELETE_USER_TRACK_HISTORY_SUCCESS';
export const DELETE_USER_TRACK_HISTORY_FAILURE = 'DELETE_USER_TRACK_HISTORY_FAILURE';

export const PUBLISH_USER_TRACK_HISTORY_REQUEST = 'PUBLISH_USER_TRACK_HISTORY_REQUEST';
export const PUBLISH_USER_TRACK_HISTORY_SUCCESS = 'PUBLISH_USER_TRACK_HISTORY_SUCCESS';
export const PUBLISH_USER_TRACK_HISTORY_FAILURE = 'PUBLISH_USER_TRACK_HISTORY_FAILURE';

export const trackHistoryRequest = () => ({type: TRACK_HISTORY_REQUEST});
export const trackHistorySuccess = trackHistory => ({type: TRACK_HISTORY_SUCCESS, payload: trackHistory});
export const trackHistoryFailure = error => ({type: TRACK_HISTORY_FAILURE, payload: error});

export const userTrackHistoryRequest = () => ({type: USER_TRACK_HISTORY_REQUEST});
export const userTrackHistorySuccess = userTracksHistory => ({type: USER_TRACK_HISTORY_SUCCESS, payload: userTracksHistory});
export const userTrackHistoryFailure = error => ({type: USER_TRACK_HISTORY_FAILURE, payload: error});

export const deleteUserTrackHistoryRequest = () => ({type: DELETE_USER_TRACK_HISTORY_REQUEST});
export const deleteUserTrackHistorySuccess = trackId => ({type: DELETE_USER_TRACK_HISTORY_SUCCESS, payload: trackId});
export const deleteUserTrackHistoryFailure = error => ({type: DELETE_USER_TRACK_HISTORY_FAILURE, payload: error});

export const publishUserTrackHistoryRequest = () => ({type: PUBLISH_USER_TRACK_HISTORY_REQUEST});
export const publishUserTrackHistorySuccess = () => ({type: PUBLISH_USER_TRACK_HISTORY_SUCCESS});
export const publishUserTrackHistoryFailure = error => ({type: PUBLISH_USER_TRACK_HISTORY_FAILURE, payload: error});

export const makeTrackHistory = (trackId, name) => {
    return async (dispatch, getState) => {
        try {
            dispatch(trackHistoryRequest());
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token,
            };
            const response = await axiosApi.post('/track_history', {track: trackId}, {headers});
            dispatch(trackHistorySuccess(response.data));
            toast.success(`Successfully added ${name} to track history`);
        } catch (error) {
            dispatch(trackHistoryFailure(error));

            if (error.response.status === 401) {
                toast.warning('You need login');
            } else {
                toast.error('Could not add to track history');
            }
        }
    };
};

export const getUserTrackHistory = () => {
    return async dispatch => {
        try {
            dispatch(userTrackHistoryRequest());

            const response = await axiosApi.get('/track_history');
            dispatch(userTrackHistorySuccess(response.data));
        } catch (error) {
            if (!error.response) {
                return dispatch(userTrackHistoryFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(userTrackHistoryFailure(error.response.statusText));
            }
        }
    };
};

export const deleteUserTrackHistory = (trackId, trackName) => {
    return async dispatch => {
        try {
            dispatch(deleteUserTrackHistoryRequest());

            await axiosApi.delete(`/track_history/${trackId}`);
            dispatch(deleteUserTrackHistorySuccess(trackId));
            toast.success(`Track ${trackName} was deleted`, {
                autoClose: 3000,
            });
        } catch (error) {
            if (!error.response) {
                return dispatch(deleteUserTrackHistoryFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(deleteUserTrackHistoryFailure(error.response.statusText));
            }
        }
    };
};

export const publishTrackHistory = id => {
    return async dispatch => {
        try {
            dispatch(publishUserTrackHistoryRequest());

            await axiosApi.put(`/track_history/${id}`);
            dispatch(publishUserTrackHistorySuccess());
            dispatch(getUserTrackHistory());
        } catch (error) {
            if (!error.response) {
                return dispatch(publishUserTrackHistoryFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(publishUserTrackHistoryFailure(error.response.statusText));
            }
        }
    };
};