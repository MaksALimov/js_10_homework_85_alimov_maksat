import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./hisoryActions";

export const GET_ALBUM_TRACKS_REQUEST = 'GET_ALBUM_TRACKS_REQUEST';
export const GET_ALBUM_TRACKS_SUCCESS = 'GET_ALBUM_TRACKS_SUCCESS';
export const GET_ALBUM_TRACKS_FAILURE = 'GET_ALBUM_TRACKS_FAILURE';

export const ADD_TRACK_REQUEST = 'ADD_TRACK_REQUEST';
export const ADD_TRACK_SUCCESS = 'ADD_TRACK_SUCCESS';
export const ADD_TRACK_FAILURE = 'ADD_TRACK_FAILURE';

export const DELETE_TRACK_REQUEST = 'DELETE_TRACK_REQUEST';
export const DELETE_TRACK_SUCCESS = 'DELETE_TRACK_SUCCESS';
export const DELETE_TRACK_FAILURE = 'DELETE_TRACK_FAILURE';

export const PUBLISH_TRACK_REQUEST = 'PUBLISH_TRACK_REQUEST';
export const PUBLISH_TRACK_SUCCESS = 'PUBLISH_TRACK_SUCCESS';
export const PUBLISH_TRACK_FAILURE = 'PUBLISH_TRACK_FAILURE';

export const getAlbumTracksRequest = () => ({type: GET_ALBUM_TRACKS_REQUEST});
export const getAlbumTracksSuccess = albumTracks => ({type: GET_ALBUM_TRACKS_SUCCESS, payload: albumTracks});
export const getAlbumTracksFailure = error => ({type: GET_ALBUM_TRACKS_FAILURE, payload: error});

export const addTrackRequest = () => ({type: ADD_TRACK_REQUEST});
export const addTrackSuccess = track => ({type: ADD_TRACK_SUCCESS, payload: track});
export const addTrackFailure = error => ({type: ADD_TRACK_FAILURE, payload: error});

export const deleteTrackRequest = () => ({type: DELETE_TRACK_REQUEST});
export const deleteTrackSuccess = trackId => ({type: DELETE_TRACK_SUCCESS, payload: trackId});
export const deleteTrackFailure = error => ({type: DELETE_TRACK_FAILURE, payload: error});

export const publishTrackSuccess = () => ({type: PUBLISH_TRACK_SUCCESS});
export const publishTrackRequest = () => ({type: PUBLISH_TRACK_REQUEST});
export const publishTrackFailure = error => ({type: PUBLISH_TRACK_FAILURE, payload: error});

export const getAlbumTracks = albumId => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token,
            };
            dispatch(getAlbumTracksRequest());
            const response = await axiosApi.get(`/tracks${albumId}`, {headers});
            dispatch(getAlbumTracksSuccess(response.data));

            if (response.data.length === 0) {
                toast.warning('This album doesn\'t have tracks yet.');
            }

        } catch (error) {

            if (!error.response) {
                return dispatch(getAlbumTracksFailure(error.message));
            }

            if (error.response.status === 401) {
                dispatch(getAlbumTracksFailure(error.response.statusText));
                toast.error('You need login');
            } else {
                toast.error('Could not load tracks');
            }

            dispatch(getAlbumTracksFailure(error.response.statusText));
        }
    };
};

export const addTrack = trackData => {
    return async dispatch => {
        try {
            dispatch(addTrackRequest());

            const response = await axiosApi.post('/tracks', trackData);
            dispatch(addTrackSuccess(response.data));
            toast.success('Track added', {
                autoClose: 3000,
            });
            dispatch(historyPush('/'));
        } catch (error) {
            if (!error.response) {
                return dispatch(addTrackFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(addTrackFailure(error.response.data));
            }

            if (error.response.status === 404) {
                dispatch(addTrackFailure(error.response.statusText));
            }
        }
    };
};

export const deleteTrack = (trackId, name) => {
    return async dispatch => {
        try {
            dispatch(deleteTrackRequest());

            await axiosApi.delete(`/tracks/${trackId}`);
            dispatch(deleteTrackSuccess(trackId));
            toast.success(`Deleted ${name}`, {
                autoClose: 3000,
            });
        } catch (error) {
            if (!error.response) {
                return dispatch(deleteTrackFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(deleteTrackFailure(error.response.statusText));
                toast.error('Could not delete track. Make sure the path is correct');
            }
        }
    };
};

export const publishTrack = (trackId, albumId) => {
    return async dispatch => {
        try {
            dispatch(publishTrackRequest());

            await axiosApi.put(`/tracks/${trackId}`);
            dispatch(publishTrackSuccess());
            dispatch(getAlbumTracks(albumId));
        } catch (error) {
            if (!error.response) {
                return dispatch(publishTrackFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(publishTrackFailure(error.response.statusText));
                toast.error('Could not publish track. Make sure the path is correct');
            }
        }
    }
};