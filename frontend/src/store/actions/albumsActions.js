import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./hisoryActions";

export const GET_ARTIST_ALBUMS_REQUEST = 'GET_ARTIST_ALBUMS_REQUEST';
export const GET_ARTIST_ALBUMS_SUCCESS = 'GET_ARTIST_ALBUMS_SUCCESS';
export const GET_ARTIST_ALBUMS_FAILURE = 'GET_ARTIST_ALBUMS_FAILURE';

export const ADD_ALBUM_REQUEST = 'ADD_ALBUM_REQUEST';
export const ADD_ALBUM_SUCCESS = 'ADD_ALBUM_SUCCESS';
export const ADD_ALBUM_FAILURE = 'ADD_ALBUM_FAILURE';

export const GET_ALBUMS_REQUEST = 'GET_ALBUMS_REQUEST';
export const GET_ALBUMS_SUCCESS = 'GET_ALBUMS_SUCCESS';
export const GET_ALBUMS_FAILURE = 'GET_ALBUMS_FAILURE';

export const DELETE_ALBUM_REQUEST = 'DELETE_ALBUM_REQUEST';
export const DELETE_ALBUM_SUCCESS = 'DELETE_ALBUM_SUCCESS';
export const DELETE_ALBUM_FAILURE = 'DELETE_ALBUM_FAILURE';

export const PUBLISH_ALBUM_REQUEST = 'PUBLISH_ALBUM_REQUEST';
export const PUBLISH_ALBUM_SUCCESS = 'PUBLISH_ALBUM_SUCCESS';
export const PUBLISH_ALBUM_FAILURE = 'PUBLISH_ALBUM_FAILURE';

export const getArtistAlbumsRequest = () => ({type: GET_ARTIST_ALBUMS_REQUEST});
export const getArtistAlbumsSuccess = artistAlbums => ({type: GET_ARTIST_ALBUMS_SUCCESS, payload: artistAlbums});
export const getArtistAlbumsFailure = error => ({type: GET_ARTIST_ALBUMS_FAILURE, payload: error});

export const addAlbumRequest = () => ({type: ADD_ALBUM_REQUEST});
export const addAlbumSuccess = album => ({type: ADD_ALBUM_SUCCESS, payload: album});
export const addAlbumFailure = error => ({type: ADD_ALBUM_FAILURE, payload: error});

export const getAlbumsRequest = () => ({type: GET_ALBUMS_REQUEST});
export const getAlbumsSuccess = albums => ({type: GET_ALBUMS_SUCCESS, payload: albums});
export const getAlbumsFailure = error => ({type: GET_ALBUMS_FAILURE, payload: error});

export const deleteAlbumRequest = () => ({type: DELETE_ALBUM_REQUEST});
export const deleteAlbumSuccess = albumId => ({type: DELETE_ALBUM_SUCCESS, payload: albumId});
export const deletedAlbumFailure = error => ({type: DELETE_ALBUM_FAILURE, payload: error});

export const publishAlbumRequest = () => ({type: PUBLISH_ALBUM_REQUEST});
export const publishAlbumSuccess = () => ({type: PUBLISH_ALBUM_SUCCESS});
export const publishAlbumFailure = error => ({type: PUBLISH_ALBUM_FAILURE, payload: error});

export const getAlbums = () => {
    return async dispatch => {
        try {
            dispatch(getAlbumsRequest());

            const response = await axiosApi.get('/albums');
            dispatch(getAlbumsSuccess(response.data));
        } catch (error) {
            dispatch(getAlbumsFailure(error.response.data));
        }
    };
};

export const getArtistAlbums = artistId => {
    return async dispatch => {
        try {
            dispatch(getArtistAlbumsRequest());

            const response = await axiosApi.get(`/albums/${artistId}`);
            dispatch(getArtistAlbumsSuccess(response.data));

            if (response.data.length === 0) {
                toast.warning('This artist doesn\'t have an album yet');
            }

        } catch (error) {
            if (!error.response) {
                return dispatch(getArtistAlbumsFailure(error.message));
            }

            if (error.response.status === 404) {
                dispatch(getArtistAlbumsFailure(error.response.statusText));
                toast.error('Albums not found.');
            } else {
                toast.error('Could not load artists');
            }
        }
    };
};

export const addAlbum = albumData => {
    return async dispatch => {
        try {
            dispatch(addAlbumRequest());

            const response = await axiosApi.post('/albums', albumData);
            dispatch(addAlbumSuccess(response.data));
            toast.success('Album added', {
                autoClose: 3000,
            });
            dispatch(historyPush('/'));

        } catch (error) {
            if (!error.response) {
                return dispatch(addAlbumFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(addAlbumFailure(error.response.data));
            }

            if (error.response.status === 404) {
                dispatch(addAlbumFailure(error.response.statusText));
            }
        }
    };
};

export const deleteAlbum = albumId => {
    return async dispatch => {
        try {
            dispatch(deleteAlbumRequest());

            await axiosApi.delete(`/albums/${albumId}`);
            dispatch(deleteAlbumSuccess(albumId));
        } catch (error) {
            if (!error.response) {
                return dispatch(deletedAlbumFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(deletedAlbumFailure(error.response.statusText));
                toast.error('Could not delete album. Make sure the path is correct');
            }
        }
    };
};

export const publishAlbum = (albumId, artistId) => {
    return async dispatch => {
        try {
            dispatch(publishAlbumRequest());
            await axiosApi.put(`/albums/${albumId}`);
            dispatch(publishAlbumSuccess());
            dispatch(getArtistAlbums(artistId));
        } catch (error) {
            if (!error.response) {
               return dispatch(publishAlbumFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(publishAlbumFailure(error.response.statusText));
            }

        }
    }
};
