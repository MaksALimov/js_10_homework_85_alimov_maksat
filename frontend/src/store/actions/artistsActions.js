import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./hisoryActions";

export const GET_ARTISTS_REQUEST = 'GET_ARTISTS_REQUEST';
export const GET_ARTISTS_SUCCESS = 'GET_ARTISTS_SUCCESS';
export const GET_ARTISTS_FAILURE = 'GET_ARTISTS_FAILURE';

export const ADD_ARTIST_REQUEST = 'ADD_ARTIST_REQUEST';
export const ADD_ARTIST_SUCCESS = 'ADD_ARTIST_SUCCESS';
export const ADD_ARTIST_FAILURE = 'ADD_ARTIST_FAILURE';

export const DELETE_ARTIST_REQUEST = 'DELETE_ARTIST_REQUEST';
export const DELETE_ARTIST_SUCCESS = 'DELETE_ARTIST_SUCCESS';
export const DELETE_ARTIST_FAILURE = 'DELETE_ARTIST_FAILURE';

export const PUBLISH_ARTIST_REQUEST = 'PUBLISH_ARTIST_REQUEST';
export const PUBLISH_ARTIST_SUCCESS = 'PUBLISH_ARTIST_SUCCESS';
export const PUBLISH_ARTIST_FAILURE = 'PUBLISH_ARTIST_FAILURE';

export const getArtistsRequest = () => ({type: GET_ARTISTS_REQUEST});
export const getArtistsSuccess = artists => ({type: GET_ARTISTS_SUCCESS, payload: artists});
export const getArtistsFailure = error => ({type: GET_ARTISTS_FAILURE, payload: error});

export const addArtistRequest = () => ({type: ADD_ARTIST_REQUEST});
export const addArtistSuccess = artist => ({type: ADD_ARTIST_SUCCESS, payload: artist});
export const addArtistFailure = error => ({type: ADD_ARTIST_FAILURE, payload: error});

export const deleteArtistRequest = () => ({type: DELETE_ARTIST_REQUEST});
export const deleteArtistSuccess = artistId => ({type: DELETE_ARTIST_SUCCESS, payload: artistId});
export const deleteArtistFailure = error => ({type: DELETE_ARTIST_FAILURE, payload: error});

export const publishArtistRequest = () => ({type: PUBLISH_ARTIST_REQUEST});
export const publishArtistSuccess = () => ({type: PUBLISH_ARTIST_SUCCESS});
export const publishArtistFailure = error => ({type: PUBLISH_ARTIST_FAILURE, payload: error});

export const getArtists = () => {
    return async dispatch => {
        try {
            dispatch(getArtistsRequest());

            const response = await axiosApi.get('/artists');
            dispatch(getArtistsSuccess(response.data));
        } catch (error) {
            if (!error.response) {
                return dispatch(getArtistsFailure(error.message));
            }

            if (error.response.status === 404) {
                dispatch(getArtistsFailure(error.response.statusText));
                toast.error('Artists not found');
            } else {
                toast.error('Could not load artists');
            }
        }
    };
};

export const addArtist = artistData => {
    return async dispatch => {
        try {
            dispatch(addArtistRequest());

            const response = await axiosApi.post('/artists', artistData);
            dispatch(addArtistSuccess(response.data));
            toast.success('Artist added!', {
                autoClose: 3000,
            });
            dispatch(historyPush('/'));
        } catch (error) {
            if (!error.response) {
                return dispatch(addArtistFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(addArtistFailure(error.response.data));
            }

            if (error.response.status === 404) {
                dispatch(addArtistFailure(error.response.statusText));
            }

        }
    };
};

export const deleteArtist = (artistId, name) => {
    return async dispatch => {
        try {
            dispatch(deleteArtistRequest());

            await axiosApi.delete(`/artists/${artistId}`);
            dispatch(deleteArtistSuccess(artistId));
            toast.success(`You successfully deleted ${name}`, {
               autoClose: 3000,
            });
        } catch (error) {
            if (!error.response) {
                return dispatch(deleteArtistFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(deleteArtistFailure(error.response.statusText));
            }
        }
    };
};

export const publishArtist = artistId => {
    return async dispatch => {
        try {
            dispatch(publishArtistRequest());
            await axiosApi.put(`/artists/${artistId}`);
            dispatch(publishArtistSuccess());
            dispatch(getArtists());
        } catch (error) {
            if (!error.response) {
                return dispatch(publishArtistFailure(error.message));
            }

            if (error.response && error.response.data) {
                dispatch(publishArtistFailure(error.response.statusText));
            }

        }
    }
};

