import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import artistsReducer from "./reducers/artistsReducer";
import tracksReducer from "./reducers/tracksReducer";
import albumsReducer from "./reducers/albumsReducer";
import usersReducer from "./reducers/usersReducer";
import trackHistoryReducer from "./reducers/trackHistoryReducer";
import thunk from "redux-thunk";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import axiosApi from "../axiosApi";

const rootReducer = combineReducers({
    artists: artistsReducer,
    tracks: tracksReducer,
    albums: albumsReducer,
    users: usersReducer,
    trackHistory: trackHistoryReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(
    applyMiddleware(thunk)
));

store.subscribe(() => {
    saveToLocalStorage({
        users: store.getState().users,
    });
});

axiosApi.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().users.user.token
    } catch (e) {}

    return config;
});

export default store;
