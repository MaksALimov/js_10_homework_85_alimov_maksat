import {
    ADD_TRACK_FAILURE,
    ADD_TRACK_REQUEST, ADD_TRACK_SUCCESS, DELETE_TRACK_FAILURE, DELETE_TRACK_REQUEST, DELETE_TRACK_SUCCESS,
    GET_ALBUM_TRACKS_FAILURE,
    GET_ALBUM_TRACKS_REQUEST,
    GET_ALBUM_TRACKS_SUCCESS, PUBLISH_TRACK_FAILURE, PUBLISH_TRACK_REQUEST, PUBLISH_TRACK_SUCCESS
} from "../actions/tracksActions";
import {CLEAR_TEXT_FIELDS_ERRORS} from "../actions/usersActions";

const initialState = {
    albumTracks: [],
    loading: null,
    error: null,
    addTrackLoading: null,
    addTrackError: null,
    track: null,
    deleteTrackLoading: null,
    deleteTrackError: null,
    publishTrackLoading: null,
    publishTrackError: null,
};

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALBUM_TRACKS_REQUEST:
            return {...state, loading: true};

        case GET_ALBUM_TRACKS_SUCCESS:
            return {...state, albumTracks: action.payload, error: false, loading: false};

        case GET_ALBUM_TRACKS_FAILURE:
            return {...state, error: action.payload, loading: false};

        case ADD_TRACK_REQUEST:
            return {...state, addTrackLoading: true, addTrackError: null};

        case ADD_TRACK_SUCCESS:
            return {...state, track: action.payload, addTrackLoading: false, addTrackError: null};

        case ADD_TRACK_FAILURE:
            return {...state, addTrackError: action.payload, addTrackLoading: false};

        case DELETE_TRACK_REQUEST:
            return {...state, deleteTrackLoading: true, deleteTrackError: false};

        case DELETE_TRACK_SUCCESS:
            return {...state, albumTracks: state.albumTracks.filter(track => track._id !== action.payload), deleteTrackLoading: false};

        case DELETE_TRACK_FAILURE:
            return {...state, deleteTrackLoading: false, deleteTrackError: action.payload};

        case PUBLISH_TRACK_REQUEST:
            return {...state, publishTrackLoading: true, publishTrackError: null};

        case PUBLISH_TRACK_SUCCESS:
            return {...state, publishTrackLoading: false, publishTrackError: false};

        case PUBLISH_TRACK_FAILURE:
            return {...state, publishTrackLoading: false, publishTrackError: action.payload};

        case CLEAR_TEXT_FIELDS_ERRORS:
            return {...state, addTrackError: null};

        default:
            return state;
    }
};

export default tracksReducer;