import {
    GET_ARTIST_ALBUMS_REQUEST,
    GET_ARTIST_ALBUMS_SUCCESS,
    GET_ARTIST_ALBUMS_FAILURE,
    ADD_ALBUM_REQUEST,
    ADD_ALBUM_SUCCESS,
    ADD_ALBUM_FAILURE,
    GET_ALBUMS_REQUEST,
    GET_ALBUMS_SUCCESS,
    GET_ALBUMS_FAILURE,
    DELETE_ALBUM_REQUEST,
    DELETE_ALBUM_SUCCESS,
    DELETE_ALBUM_FAILURE,
    PUBLISH_ALBUM_REQUEST, PUBLISH_ALBUM_SUCCESS, PUBLISH_ALBUM_FAILURE
} from "../actions/albumsActions";
import {CLEAR_TEXT_FIELDS_ERRORS} from "../actions/usersActions";

const initialState = {
    error: null,
    artistAlbums: [],
    artistAlbumsLoading: null,
    addAlbumLoading: null,
    addAlbumError: null,
    album: null,
    getAlbumsLoading: null,
    albums: [],
    getAlbumsError: null,
    deleteAlbumLoading: null,
    deleteAlbumError: null,
    publishAlbumLoading: null,
    publishAlbumError: null,
};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ARTIST_ALBUMS_REQUEST:
            return {...state, artistAlbumsLoading: true};

        case GET_ARTIST_ALBUMS_SUCCESS:
            return {...state, artistAlbums: action.payload, artistAlbumsLoading: false, error: false};

        case GET_ARTIST_ALBUMS_FAILURE:
            return {...state, error: action.payload, artistAlbumsLoading: false};

        case ADD_ALBUM_REQUEST:
            return {...state, addAlbumLoading: true, addAlbumError: null};

        case ADD_ALBUM_SUCCESS:
            return {...state, album: action.payload, addAlbumLoading: false};

        case ADD_ALBUM_FAILURE:
            return {...state, addAlbumError: action.payload, addAlbumLoading: false};

        case GET_ALBUMS_REQUEST:
            return {...state, getAlbumsLoading: true, getAlbumsError: null};

        case GET_ALBUMS_SUCCESS:
            return {...state, albums: action.payload, getAlbumsLoading: false};

        case GET_ALBUMS_FAILURE:
            return {...state, getAlbumsError: action.payload, getAlbumsLoading: false};

        case DELETE_ALBUM_REQUEST:
            return {...state, deleteAlbumLoading: true, deleteAlbumError: null};

        case DELETE_ALBUM_SUCCESS:
            return {...state, artistAlbums: state.artistAlbums.filter(album => album._id !== action.payload), deleteAlbumLoading: false, deleteAlbumError: null};

        case DELETE_ALBUM_FAILURE:
            return {...state, deleteAlbumError: action.payload, deleteAlbumLoading: false};

        case PUBLISH_ALBUM_REQUEST:
            return {...state, publishAlbumLoading: true, publishAlbumError: false};

        case PUBLISH_ALBUM_SUCCESS:
            return {...state, publishAlbumLoading: false, publishAlbumError: false};

        case PUBLISH_ALBUM_FAILURE:
            return {...state, publishAlbumLoading: false, publishAlbumError: action.payload};

        case CLEAR_TEXT_FIELDS_ERRORS:
            return {...state, addAlbumError: null};

        default:
            return state;
    }
};

export default albumsReducer;