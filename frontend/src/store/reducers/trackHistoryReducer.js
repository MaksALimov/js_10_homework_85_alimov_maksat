import {
    DELETE_USER_TRACK_HISTORY_FAILURE,
    DELETE_USER_TRACK_HISTORY_REQUEST,
    DELETE_USER_TRACK_HISTORY_SUCCESS, PUBLISH_USER_TRACK_HISTORY_FAILURE,
    PUBLISH_USER_TRACK_HISTORY_REQUEST,
    PUBLISH_USER_TRACK_HISTORY_SUCCESS,
    TRACK_HISTORY_FAILURE,
    TRACK_HISTORY_REQUEST,
    TRACK_HISTORY_SUCCESS,
    USER_TRACK_HISTORY_FAILURE,
    USER_TRACK_HISTORY_REQUEST,
    USER_TRACK_HISTORY_SUCCESS
} from "../actions/trackHistoryActions";

const initialState = {
    error: null,
    trackHistoryLoading: null,
    trackHistory: null,
    userTrackHistory: [],
    userTrackHistoryLoading: null,
    userTrackHistoryError: null,
    deleteUserTrackHistoryLoading: null,
    deleteUserTrackHistoryError: null,
    publishUserTrackHistoryLoading: null,
    publishUserTrackHistoryError: null
};

const trackHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case TRACK_HISTORY_REQUEST:
            return {...state, trackHistoryLoading: true};

        case TRACK_HISTORY_SUCCESS:
            return {...state, trackHistory: action.payload, trackHistoryLoading: false};

        case TRACK_HISTORY_FAILURE:
            return {...state, error: action.payload, trackHistoryLoading: false};

        case USER_TRACK_HISTORY_REQUEST:
            return {...state, userTrackHistoryLoading: true};

        case USER_TRACK_HISTORY_SUCCESS:
            return {...state, userTrackHistory: action.payload, userTrackHistoryLoading: false};

        case USER_TRACK_HISTORY_FAILURE:
            return {...state, userTrackHistoryError: action.payload, userTrackHistoryLoading: false};

        case DELETE_USER_TRACK_HISTORY_REQUEST:
            return {...state, deleteUserTrackHistoryLoading: true, deleteUserTrackHistoryError: false};

        case DELETE_USER_TRACK_HISTORY_SUCCESS:
            return {
                ...state,
                userTrackHistory: state.userTrackHistory.filter(track => track._id !== action.payload),
                deleteUserTrackHistoryLoading: false,
                deleteUserTrackHistoryError: false,
            };

        case DELETE_USER_TRACK_HISTORY_FAILURE:
            return {...state, userTrackHistoryLoading: false, deleteUserTrackHistoryError: action.payload, deleteUserTrackHistoryLoading: false};

        case PUBLISH_USER_TRACK_HISTORY_REQUEST:
            return {...state, publishUserTrackHistoryLoading: true, publishUserTrackHistoryError: false};

        case PUBLISH_USER_TRACK_HISTORY_SUCCESS:
            return {...state, publishUserTrackHistoryLoading: false, deleteUserTrackHistoryError: false};

        case PUBLISH_USER_TRACK_HISTORY_FAILURE:
            return {...state, publishUserTrackHistoryLoading: false, publishUserTrackHistoryError: action.payload};

        default:
            return state;
    }
};

export default trackHistoryReducer;