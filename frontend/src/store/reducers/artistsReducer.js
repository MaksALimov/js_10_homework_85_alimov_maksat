import {
    ADD_ARTIST_FAILURE,
    ADD_ARTIST_REQUEST, ADD_ARTIST_SUCCESS, DELETE_ARTIST_FAILURE, DELETE_ARTIST_REQUEST, DELETE_ARTIST_SUCCESS,
    GET_ARTISTS_FAILURE,
    GET_ARTISTS_REQUEST,
    GET_ARTISTS_SUCCESS, PUBLISH_ARTIST_FAILURE, PUBLISH_ARTIST_REQUEST, PUBLISH_ARTIST_SUCCESS
} from "../actions/artistsActions";
import {CLEAR_TEXT_FIELDS_ERRORS} from "../actions/usersActions";

const initialState = {
    artists: [],
    artistsLoading: null,
    error: null,
    addArtist: null,
    addArtistLoading: null,
    addArtistError: null,
    deleteArtistLoading: null,
    deleteArtistError: null,
    publishArtistLoading: null,
    publishArtistError: null,
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ARTISTS_REQUEST:
            return {...state, artistsLoading: true};

        case GET_ARTISTS_SUCCESS:
            return {...state, artists: action.payload, artistsLoading: false, error: false};

        case GET_ARTISTS_FAILURE:
            return {...state, error: action.payload, artistsLoading: false};

        case ADD_ARTIST_REQUEST:
            return {...state, addArtistError: null, addArtistLoading: true};

        case ADD_ARTIST_SUCCESS:
            return {...state, addArtist: action.payload, addArtistLoading: false};

        case ADD_ARTIST_FAILURE:
            return {...state, addArtistError: action.payload, addArtistLoading: false};

        case DELETE_ARTIST_REQUEST:
            return {...state, deleteArtistLoading: true, deleteArtistError: null};

        case DELETE_ARTIST_SUCCESS:
            return {...state, artists: state.artists.filter(artist => artist._id !== action.payload), deleteArtistLoading: false};

        case DELETE_ARTIST_FAILURE:
            return {...state, deleteArtistLoading: false, deleteArtistError: action.payload};

        case PUBLISH_ARTIST_REQUEST:
            return {...state, publishArtistLoading: true, publishArtistError: false};

        case PUBLISH_ARTIST_SUCCESS:
            return {...state, publishArtistLoading: false, publishArtistError: false};

        case PUBLISH_ARTIST_FAILURE:
            return {...state, publishArtistLoading: false, publishArtistError: action.payload};

        case CLEAR_TEXT_FIELDS_ERRORS:
            return {...state, addArtistError: null};

        default:
            return state
    }
};

export default artistsReducer;