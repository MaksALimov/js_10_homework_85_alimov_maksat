export const saveToLocalStorage = state => {
    try {
        const serialized = JSON.stringify(state);
        localStorage.setItem('shopState', serialized);
    } catch (e) {
        console.log('Could not save state');
    }
};

export const loadFromLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('shopState');

        if (serializedState === null) {
            return undefined;
        }

        return JSON.parse(serializedState);

    } catch (e) {
        return undefined;
    }
};
